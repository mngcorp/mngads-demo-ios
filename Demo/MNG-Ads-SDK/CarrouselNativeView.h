//
//  CarrouselNetiveView.h
//  MNG-Ads-SDK-Demo
//
//  Created by Ben Salah Med Amine on 4/7/15.
//  Copyright (c) 2015 MNG. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CarrouselNativeView : UIView

@property UIImageView *backgroundImage;
@property UIImageView *iconeImage;
@property UILabel *titleLabel;
@property UILabel *descriptionLabel;
@property UIButton *callToActionButton;

@end
