//
//  GADBlueStackMediationAdapter.h
//  MNG-Ads-SDK-Demo
//
//  Created by anypli on 8/3/2022.
//  Copyright © 2022 Bensalah Med Amine. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <GoogleMobileAds/GoogleMobileAds.h>


@interface GADBlueStackMediationAdapter : NSObject
+(void)setViewController:(UIViewController*)viewController;

@end

