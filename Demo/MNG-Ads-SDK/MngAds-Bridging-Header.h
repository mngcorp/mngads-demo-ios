//
//  MngAds-Bridging-Header.h
//  MNG-Ads-SDK-Demo
//
//  Created by Ben Salah Med Amine on 4/14/15.
//  Copyright (c) 2015 MNG. All rights reserved.
//

#import "AppDelegate.h"
#import <BlueStackSDK/MNGAdsSDKFactory.h>
#import "MNGAdsConfig.h"
#import "MNGAdsAppId.h"
#import <BlueStackSDK/MNGNAtiveObject.h>
#import "Utils.h"

