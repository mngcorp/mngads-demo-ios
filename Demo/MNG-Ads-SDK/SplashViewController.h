//
//  SplashViewController.h
//  MNG-Ads-SDK-Demo
//
//  Created by Hussein Dimessi on 15/02/2018.
//  Copyright © 2018 MAdvertise. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SplashViewController : UIViewController<MNGAdsAdapterInterstitialDelegate,MNGClickDelegate, MNGAdsSDKFactoryDelegate>

@end
