//
//  Utils.h
//  MNG-Ads-SDK-Demo
//
//  Created by Ben Salah Med Amine on 8/11/15.
//  Copyright (c) 2015 MNG. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Utils : NSObject

+(void)displayToastWithMessage:(NSString *)toastMessage;
+(MNGPreference *)getTestPreferences;
@end
