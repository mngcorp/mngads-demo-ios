//
//  RewardVideoViewController.h
//  MNG-Ads-SDK-Demo
//
//  Created by Hussein Dimessi on 06/09/2017.
//  Copyright © 2017 MAdvertise. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RewardVideoViewController : UIViewController <MAdvertiseAdapterRewardedVideoAdDelegate>

@end
