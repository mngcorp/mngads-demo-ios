//
//  MNGAdsAppId.h
//  MNG-Ads-SDK-Demo
//
//  Created by Yecine Dhouib on 01/06/15.
//  Copyright (c) 2015 Mng. All rights reserved.
//

#ifndef MNG_Ads_SDK_Demo_MNGAdsAppId_h
#define MNG_Ads_SDK_Demo_MNGAdsAppId_h

#define DEFAULT_MNG_ADS_APP_ID @""
//dfp adapter keys

#define BANNER_AD_ADUNIT @""
#define INTERSTITIEL_AD_ADUNIT @""
#define SQUARE_AD_ADUNIT @""
#define NATIVE_AD_ADUNIT @""
#define REWARD_AD_ADUNIT @""
#endif
