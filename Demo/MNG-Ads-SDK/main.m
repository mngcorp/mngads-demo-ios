//
//  main.m
//  MNG-Ads-SDK
//
//  Created by Mng on 12/9/14.
//  Copyright (c) 2014 Mng. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
  return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
