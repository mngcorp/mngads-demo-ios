//
//  MngAdsSwiftConfig.swift
//  MNG-Ads-SDK-Demo
//
//  Created by Hussein Dimessi on 3/21/17.
//  Copyright © 2017 MAdvertise. All rights reserved.
//

var MNG_ADS_APP_ID = "testtt"



struct PLACEMENTS {
    
    static let MNG_ADS_BANNER_PLACEMENT_ID = "/\(MNG_ADS_APP_ID)/banner"
    static let MNG_ADS_NATIVEAD_PLACEMENT_ID = "/\(MNG_ADS_APP_ID)/nativead"
    static let MNG_ADS_INTERSTITIAL_PLACEMENT_ID = "/\(MNG_ADS_APP_ID)/interstitial"
    static let MNG_ADS_INTERSTITIAL_OVERLAY_PLACEMENT_ID = "/\(MNG_ADS_APP_ID)/interstitialOverlay"
    static let MNG_ADS_INFEED_PLACEMENT_ID = "/\(MNG_ADS_APP_ID)/infeed"
    static let MNG_ADS_THUMBNAIL_PLACEMENT_ID = "/\(MNG_ADS_APP_ID)/thumbnailOgury"


}
