![mad_AdNetworkMediation_rgb_small.png](https://bitbucket.org/repo/GyRXRR/images/3981639300-mad_AdNetworkMediation_rgb_small.png) for IOS

# BlueStack Mediation SDK for IOS

Madvertise provides functionalities for monetizing your mobile application: from premium sales with rich media, video and innovative formats, it facilitates inserting native mobile ads as well all standard display formats. MngAds SDK (BlueStack) is a library that allow you to handle Ads servers with the easy way.


| **SDK Integration** | 
| -------- |
| [Prepare for iOS 14+] |
| [Setup the IOS SDK] | 
| [IOS Change Log] 
| [IOS Upgrade Guide] 


| **Ad Formats** | 
| -------- |
| [IOS Banner Ads] | 
| [IOS Interstitial Ads] |
| [IOS Infeed Ads] |
| [IOS Native Ads] | 
| [IOS Rewarded Video Ads]| 
| [IOS Thumbnail Ads]| 



| **Advanced Settings** | 
| -------- |
|[IOS FAQ and Troubleshooting]
|[IOS Debug Mode with Gyroscope Sensor]
|[IOS Targeting Audiences] 


| **Mediation Adapters** | 
| -------- | 
| [IOS Google Mobile Ads Adapter]
| [IOS Mopub Adapter]



[Setup the IOS SDK]:https://bitbucket.org/mngcorp/mngads-demo-ios/wiki/setup
[IOS Change Log]:https://bitbucket.org/mngcorp/mngads-demo-ios/wiki/change-log
[IOS Targeting Audiences]:https://bitbucket.org/mngcorp/mngads-demo-ios/wiki/targeting-audiences
[IOS Native Ads]:https://bitbucket.org/mngcorp/mngads-demo-ios/wiki/nativead
[IOS Upgrade Guide]:https://bitbucket.org/mngcorp/mngads-demo-ios/wiki/upgrading
[IOS FAQ and Troubleshooting]:https://bitbucket.org/mngcorp/mngads-demo-ios/wiki/faq
[IOS Best practices]:https://bitbucket.org/mngcorp/mngads-demo-ios/wiki/guidelines
[IOS Debug Mode with Gyroscope Sensor]:https://bitbucket.org/mngcorp/mngads-demo-ios/wiki/debug-mode-gyro
[IOS Mopub Adapter]:https://bitbucket.org/mngcorp/mobile.mng-ads.com-mngperf/wiki/mopub-adaptor-ios
[IOS Rewarded Video Ads]:https://bitbucket.org/mngcorp/mngads-demo-ios/wiki/rewarded-video-ios
[IOS Infeed Ads]:https://bitbucket.org/mngcorp/mngads-demo-ios/wiki/infeed
[IOS Banner Ads]:https://bitbucket.org/mngcorp/mngads-demo-ios/wiki/banner
[IOS Interstitial Ads]:https://bitbucket.org/mngcorp/mngads-demo-ios/wiki/interstitial
[IOS Google Mobile Ads Adapter]:https://bitbucket.org/mngcorp/mngads-demo-ios/wiki/dfp-adapter-ios
[IOS Thumbnail Ads]:https://bitbucket.org/mngcorp/mngads-demo-ios/wiki/thumbnail
[Prepare for iOS 14+]:https://bitbucket.org/mngcorp/mngads-demo-ios/wiki/ios14